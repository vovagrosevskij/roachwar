// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RoachWarGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ROACHWAR_API ARoachWarGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
