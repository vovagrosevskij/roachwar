#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/Texture2D.h"
#include "RW_Types.generated.h"


UENUM(BlueprintType)
enum class ERoachType : uint8
{
	Click_Type UMETA(DisplayName = "Click_Type"),
	AutoClick_Type UMETA(DisplayName = "AutoClick_Type"),
	Gold_Type UMETA(DisplayName = "Gold_Type"),
};

UCLASS()
class ROACHWAR_API URW_Types : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

};